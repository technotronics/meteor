export const publicRoutes = FlowRouter.group({
	name:'publicRoutes'
});

export const privateRoutes = FlowRouter.group({
	name:'privateRoutes',
	prefix:'/admin',
	triggersEnter:[
		function(obj,redirect){
			if (!Meteor.userId()){
				Bert.alert('Você precisa fazer o login para ter permissão de acesso.','warning','growl-top-right');
				redirect('loginRoute');
			}
		}
	]
});
