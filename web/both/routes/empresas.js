import { privateRoutes } from './groups.js';

privateRoutes.route('/empresas',{
	name: 'empresasRoute',
	action: function() {
		BlazeLayout.render('defaultLayout',{
			header:'topHeaderView',
			main:'empresasView'
		});
	}
});

privateRoutes.route('/empresas/novo',{
	name: 'empresasInsertRoute',
	action: function() {
		BlazeLayout.render('defaultLayout',{
			header:'topHeaderView',
			main:'empresasFormView'
		});
	}
});

privateRoutes.route('/empresas/edita/:id',{
	name: 'empresasUpdateRoute',
	action: function() {
		BlazeLayout.render('defaultLayout',{
			header:'topHeaderView',
			main:'empresasFormView'
		});
	}
});
