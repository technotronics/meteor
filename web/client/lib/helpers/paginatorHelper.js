Template.registerHelper("paginatorHelper", function(args){
	var itemsPerPage = Meteor.settings.public.paginator.itemsPerPage;

	var count = args.hash.count;
	var page = (FlowRouter.getQueryParam('page')?FlowRouter.getQueryParam('page'):1);
	var qtd = Math.ceil(count/itemsPerPage);

	var pages = [];

	for(x=1;x<=qtd;x++){
		pages.push({
			page:x,
			active:(page==x?'active':''),
			disabled:(page==x?'readonly':'')
		});
	}

	return pages;
});

Template.registerHelper("lastPage", function(args){
	var itemsPerPage = Meteor.settings.public.paginator.itemsPerPage;
	var count = args.hash.count;
	var page = (FlowRouter.getQueryParam('page')?FlowRouter.getQueryParam('page'):1);
	var qtd = Math.ceil(count/itemsPerPage);
	return '?page='+qtd;
});
